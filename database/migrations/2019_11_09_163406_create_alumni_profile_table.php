<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlumniProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumni_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');

            $table->string('first_name', 32);
            $table->string('last_name', 32)->nullable();
            $table->string('gelar_depan')->nullable();
            $table->string('gelar_belakang');
            $table->string('gender', 1)->comment('M/F');
            $table->string('tempat_lahir')
            $table->date('birthdate');
            $table->string('image');
            $table->unsignedInteger('angkatan_pesan_id');

            $table->timestampsTz();
            $table->softDeletesTz();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('restrict')->onDelete('restrict');
            $table->foreign('angkatan_pesan_id')->references('id')->on('angkatan_pesan')->onUpdate('restrict')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumni_profile');
    }
}
